
import {shuffleArray} from './common';

const LINE_COLORS = shuffleArray([
  '#9933cc', '#6b238e', '#3c1451',
  '#ffbb33', '#ff8800', '#cc6c00',
  '#995100', '#ffb2b2', '#ff7f7f',
  '#33b5e5', '#0099cc', '#007299',
  '#436500', '#2c4c00', '#ffe564',
  '#d2fe4c', '#99cc00', '#669900',
  '#004c66', '#bc93d1', '#aa66cc',
  '#ff4444', '#cc0000', '#7f0000',
  '#b20058', '#660033', '#8ed5f0',
  '#ff7fbf', '#ff3298', '#e50072',
]);

export { LINE_COLORS };
