import dateFormat from 'dateformat';

const fetchLastPrice = prices => {
  const lastDate = new Date(Math.max.apply(
    null,
    prices.map(e => new Date(e.date))
  ));
  return prices.filter(item => new Date(item.date).getTime() === lastDate.getTime())[0];
};

const formatTableFromPayload = (payload) => {
  const result = [];
  payload.forEach(item => {
    const tmp = {
      model: item.model,
      category: item.category
    };
    result.push(...item.shops.map(shop => {
      return Object.assign(
        {},
        shop,
        tmp,
        { lastPriceItem: fetchLastPrice(shop.prices) },
      )
    }));
  });
  console.log(JSON.stringify(result, 2, 2));
  return result;
};

const formatChartDataFromPayload = (payload) => {
  const chartMatrix = payload[0].prices.map(priceItem => {
    return {date: dateFormat(new Date(priceItem.date), 'mm-dd')}
  });
  payload.forEach(payloadItem => {
    payloadItem.prices.forEach((priceItem, priceIndex) => {
      chartMatrix[priceIndex][`${payloadItem.model} / ${payloadItem.shop}`] = priceItem.price;
    });
  });
  console.log('Chart matrix: ', chartMatrix);
  return chartMatrix;
};

export {
  formatTableFromPayload,
  formatChartDataFromPayload
};


// {
//   "prices": [
//   {
//     "_id": "5a77053a97d1bd12d00fe222",
//     "date": "1991-02-15T22:00:00.000Z",
//     "price": 1000
//   },
//   {
//     "_id": "5a77053a97d1bd12d00fe221",
//     "date": "1994-08-24T21:00:00.000Z",
//     "price": 2000
//   },
//   {
//     "_id": "5a77053a97d1bd12d00fe220",
//     "date": "1998-03-02T22:00:00.000Z",
//     "price": 3000
//   },
//   {
//     "_id": "5a77053a97d1bd12d00fe21f",
//     "date": "2001-09-08T21:00:00.000Z",
//     "price": 4000
//   }
// ],
//   "_id": "5a77053a97d1bd12d00fe21e",
//   "shop": "rozetka.com.ua",
//   "url": "https://comfy.ua/smartfon-samsung-a530f-galaxy-a8-2018-black.html",
//   "imageUrl": "https://cdn.comfy.ua/media/catalog/product/cache/4/small_image/270x265/62defc7f46f3fbfc8afcd112227d1181/d/f/dfwefwkergferfgbv-df_1.jpg",
//   "model": "Samsung W789 Galaxy Black",
//   "category": "smartphones",
//   "lastPriceItem": {
//   "_id": "5a77053a97d1bd12d00fe21f",
//     "date": "2001-09-08T21:00:00.000Z",
//     "price": 4000
// }
// }
