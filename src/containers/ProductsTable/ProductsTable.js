import React, {Component} from 'react';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import {Grid, Row, Col} from 'react-flexbox-grid';

const styles = {
  detailsButton: {
    textAlign: 'left',
    padding: 5
  }
};

class ProductsTable extends Component {

  render() {
    return (
      <Col md={6}>
        <Table
          height={'400'}
          selectable={false}
          multiSelectable={false}
          fixedHeader={true}
          // onRowSelection={ (event) => {
          //   console.log(event);
          //   console.log(this.props.getProductsDataKeys());
          //   // TODO: fix this!
          //   // this.props.getProductsList();
          //   // this.props.setChartData([]);
          //   // this.props.setProductsDataKeys([]);
          // } }
        >
          <TableHeader
            // enableSelectAll={true}
            displaySelectAll={false}
            adjustForCheckbox={false}
          >
            <TableRow>
              <TableHeaderColumn>Product name</TableHeaderColumn>
              <TableHeaderColumn>Market</TableHeaderColumn>
              <TableHeaderColumn>Last tracked price</TableHeaderColumn>
              <TableHeaderColumn>Details</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody
            // selectable={false}
            // deselectOnClickaway={false}
            displayRowCheckbox={false}
            showRowHover={true}
            stripedRows={false}
          >
            {
              (
                this.props.getProductsList().length === 0 ? [] : this.props.getProductsList()
              ).map((item, index) =>
                <TableRow
                  key={index}
                >
                  <TableRowColumn
                    style={{whiteSpace: 'pre-wrap'}}
                  >{item.model}</TableRowColumn>
                  <TableRowColumn
                    style={{whiteSpace: 'pre-wrap'}}
                  >{item.shop}</TableRowColumn>
                  <TableRowColumn
                    style={{whiteSpace: 'pre-wrap'}}
                  >{item.lastPriceItem.price}</TableRowColumn>
                  <TableRowColumn style={styles.detailsButton}>
                    <a href={item.url} target="_blank">
                      <RaisedButton
                        label="Go to shop"
                      />
                    </a>
                  </TableRowColumn>
                </TableRow>
              )
            }
          </TableBody>
        </Table>
      </Col>
    );
  }
}

export default ProductsTable;
