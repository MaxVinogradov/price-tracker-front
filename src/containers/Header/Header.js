import React from 'react';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

function handleClick() {
  alert('onClick triggered on the title component');
}

const Header = () => (
  <MuiThemeProvider>
    <AppBar
      title={
        <span>Price Tracker UA</span>
      }
      showMenuIconButton={false}
    />
  </MuiThemeProvider>
);

export default Header;
