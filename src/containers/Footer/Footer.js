import React from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';

const styles = {
  footer: {
    position: 'fixed',
    bottom: 0,
    width: '100%',
    color: 'white',
    backgroundColor: 'black'
  }
};

const Footer = () => (
  <div style={styles.footer}>
    <p>Created by Vinogradov Maxim</p>
  </div>
);

export default Footer;
