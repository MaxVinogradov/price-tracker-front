import React from 'react';
import {Col} from 'react-flexbox-grid';
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';
import {LINE_COLORS} from '../../core/constants';

const Statistic = (props) => (
  <Col md={6}>
    <ResponsiveContainer width='100%' aspect={2}>
      <LineChart data={props.getChartData()}
                 margin={{top: 5, right: 30, left: 20, bottom: 5}}>
        <CartesianGrid strokeDasharray="3 3"/>
        <XAxis dataKey="date"/>
        <YAxis/>
        <Tooltip/>
        <Legend/>
        {
          props.getProductsDataKeys().map((dataKey, index) =>
            <Line
              key={index}
              type="monotone"
              dataKey={dataKey}
              stroke={LINE_COLORS[index % LINE_COLORS.length]}
            />
          )
        }
      </LineChart>
    </ResponsiveContainer>
  </Col>
);

export default Statistic;
