import React, {Component} from 'react';
import SelectField from 'material-ui/SelectField';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import { Grid, Row, Col } from 'react-flexbox-grid';

export default class Select extends Component {

  render() {
    return (
      <Grid fluid>
        <Row center={"md"} middle={"md"}>
          <Col md={3}>
            <SelectField
              floatingLabelText="Category"
              value={this.props.getCurrentCategory()}
              onChange={this.props.handleCategoryChange}
            >
              {
                this.props.categories.map((category, index) => {
                  return <MenuItem
                    key={index}
                    value={category}
                    primaryText={category}
                  />
                })
              }
            </SelectField>
          </Col>
          <Col md={3}>
            <TextField
              hintText="Input model"
              floatingLabelText="Model"
              onChange={this.props.handleModelChange}
            />
          </Col>
        </Row>
        <br/>
        <Row center={"md"} middle={"md"}>
          <Col md={4}>
            <RaisedButton
              label="Select category and enter model and search it!"
              primary={true}
              fullWidth={true}
              onClick={this.props.searchHandler}
            />
          </Col>
        </Row>
      </Grid>
    );
  }
}
