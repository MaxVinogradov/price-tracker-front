import React, {Component} from 'react';
import axios from 'axios';
import './App.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Header from '../Header/Header';
import Search from '../Search/Search';
import ProductsTable from '../ProductsTable/ProductsTable';
import Statistic from '../Statistic/Statistic';
import {Grid, Row} from 'react-flexbox-grid';
import {formatTableFromPayload, formatChartDataFromPayload} from '../../core/payloadFormatters';

class App extends Component {
  state = {
    categories: null,
    targetCategory: null,
    targetModel: null,
    productsList: [],

    // Stuff for chart
    productsDataKeys: [],
    fullChartData: [],
    filteredChartData: [],
  };

  handleCategoryChange = (event, index, value) => this.setState({targetCategory: value});
  handleModelChange = (event, index, value) => this.setState({targetModel: event.target.value});
  getCurrentCategory = () => this.state.targetCategory;
  getCurrentModel = () => this.state.targetModel;
  getProductsList = () => this.state.productsList;
  getChartData = () => this.state.fullChartData;
  getProductsDataKeys = () => this.state.productsDataKeys;
  setChartData = (productsList) => this.setState({
    fullChartData: formatChartDataFromPayload(productsList)
  });
  setProductsDataKeys = (productsList) => this.setState({
    productsDataKeys: productsList.map(item => `${item.model} / ${item.shop}`)
  });

  searchHandler = async (val) => {
    console.log('!!!', val);
    const res = await axios.get('http://localhost:3001/api/products', {
      params: {
        category: this.state.targetCategory,
        model: this.state.targetModel
      }
    });
    this.setState({
      productsList: formatTableFromPayload(res.data.response.products)
    });
    this.setChartData(this.state.productsList);
    this.setProductsDataKeys(this.state.productsList);
    console.log('!!~~~~~~>', this.state.productsDataKeys);
  };

  setStateAsync(state) {
    return new Promise((resolve) => {
      this.setState(state, resolve);
    });
  }

  async componentDidMount() {
    const res = await axios.get('http://localhost:3001/api/categories/');
    console.log(JSON.stringify(res));
    if (res.data.response.status === 'success') {
      this.setStateAsync({categories: res.data.response.categories});
    }
  }

  render() {
    if (this.state.categories) {
      return (
        <div className="App">
          <MuiThemeProvider>
            <Header/>
            <Search
              searchHandler={this.searchHandler}
              getCurrentCategory={this.getCurrentCategory}
              getCurrentModel={this.getCurrentModel}
              handleModelChange={this.handleModelChange}
              handleCategoryChange={this.handleCategoryChange}
              categories={this.state.categories}
            />
            <br/>
            <br/>
            <Grid fluid>
              <Row>
                <ProductsTable
                  getProductsList={this.getProductsList}
                  // onTableRowSelection={this.onTableRowSelection}
                  getChartData={this.getChartData}
                  getProductsDataKeys={this.getProductsDataKeys}
                  setChartData={this.setChartData}
                  setProductsDataKeys={this.setProductsDataKeys}
                />
                <Statistic
                  getChartData={this.getChartData}
                  getProductsDataKeys={this.getProductsDataKeys}
                />
              </Row>
            </Grid>
          </MuiThemeProvider>
        </div>
      );
    } else {
      return (
        <div className="App">
          {/*<p>Error! Cannot fetch data from server!</p>*/}
        </div>
      );
    }
  }
}

export default App;
